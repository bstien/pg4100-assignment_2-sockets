# Om min innlevering
Min innlevering er delt i to deler: server og klient. Klienten er gjort så simpel som mulig, da jeg ikke føler det bør ligge for mye logikk i denne innleveringens klient.

## Server
Serveren har ansvaret et par forskjellige ansvar som må oppfylles for at applikasjonen skal fungere. Den er nødt til å holde kontakten med en database for å hente bøker, lytte på en gitt port for inkomne forespørsler, opprette en ny tråd for hver klient så flere kan spille samtidig og i hver av disse trådene presentere spørsmål for spilleren og sjekke hans/hennes svar.

### AppServer
Klassen `AppServer` er ansvarlig for å kickstarte applikasjonen, hente ut bøker og vente på innkommende forespørsler. Da det er helt tydelig at datagrunnlaget av bøker ikke kommer til å endres under applikasjonens kjøretid har jeg valgt å hente ut alle bøkene ved start og dermed slippe å holde en åpen databasekobling. Hadde situasjonen vært annerledes, dvs. at bøkene hadde hatt mulighet til å endre seg eller resultater skal lagres, hadde jeg gjort dette annerledes.

Innkommende forespørsler tas imot i en uendelig løkke og hver nye forespørsel får en ny tråd. For å delegere tråder har jeg tatt i bruk `Executors.newCachedThreadPool()`. Dersom det kommer flere og oftere nye forespørsler vil denne løsningen lønne seg, da tråder vil kunne gjenbrukes.

Ved hver nye forespørsel instansieres en ny instans av `QuizResponder`, som mottar den innkommende socketen.

### BookHandler
Dette er klassen som kommuniserer med `ConnectToDb`, klassen vi lagde i PG3100 i fjor høst. Klassen har én enkel oppgave: hente ut alle bøker fra databasen.

### QuizResponder
Dette er klassen som tar ansvar for selve quizzen, og dermed også kommunikasjonen med klienten. Den implementerer `Runnable` så den kan kjøres i en separat tråd. Den får listen med bøker samt en socket som argument til konstruktøren.

Spillet foregår ved at brukeren stilles spørsmål helt til han/hun velger å ikke spille lenger, dette foregår i en potensielt evig løkke.

For hver gang brukeren velger å fortsette velges det ut et vilkårlig spørsmål om en vilkårlig bok og den oppbygde spørsmålsstrengen sendes til brukeren. Svaret han/hun sender tilbake sjekkes så mot korrekt svar og tilbakemelding gis.

Klassen har et par metoder som også bør bli dratt ut i separate klasser, deriblant å sjekke om brukerens svar stemmer samt metoden `isPositive()`, som i bunn og grunn kun sjekker om brukerens svar er positivt. Altså om det er svart 'ja', 'j', 'yes', etc.

### SocketIOHandler
Denne klassen forenkler kommunikasjonen inn og ut av en gitt socket. Underliggende benytter den en `StringBuilder` for å bygge opp en respons og har en egen metode, `SocketIOHandler.send()`, som skriver innholdet til `OutputStream` og tømmer bufferen.

Enkelte av metodene returnerer objektet det tilhører slik at man enkelt kan chaine en rekke kommandoer.

Dette er en ekstremt enkel klasse, men den gjorde virkelig oppbygningen av respons enklere.

### Question
Et interface som, i min applikasjon, implementeres i to konkrete klasser: `AuthorQuestion` og `FirstPublishedQuestion`. Spørsmålene har et par faste metoder, deriblant hente ut strengen med selve spørsmålet samt mulighet for å sjekke om spillerens svar er korrekt.

Selve sjekken om svaret fra bruker stemmer er et ansvar som ikke burde vært tatt av disse klassene, men som jeg for enkelhetsskyld har valgt å beholde implementert. 

#### QuestionFactory
Dette er en klasse som tar en `List<Book>` som argument og returnerer et vilkårlig spørsmål for en vilkårlig bok via metoden `QuestionFactory.makeRandomQuestion()`.

## Klienten
Klienten min er ekstremt liten og gjør kun det nødvendige: lese og skrive til socketen. All logikk tas hånd om server-side, så da er det ikke så mye som gjenstår for klienten.

Den eneste sjekken som gjøres er om den innkommende meldingen fra server er lik `--end`. Da vi trenger en form for måte å styre hva som forventes at gjøres av enten klient eller server utifra slike meldinger er dette den enkleste måten jeg kom på at jeg kunne signalisere at server nå avslutter kommunikasjonen. Dette er også potensielt noe som bør trekkes ut i egen klasse som parser og styrer flyten i klienten basert på slike forhåndsdefinerte meldinger.