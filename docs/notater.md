# Innlevering 2: Sockets
_Levert av Bastian Stien_

## Hva har jeg lært?
Jeg har lært hvordan jeg får applikasjoner til å prate sammen over internett via sockets.

## Hva kunne vært gjort bedre?
Jeg kunne gjort meg litt mer flid å implementere et eget API mellom klient og server. Den eneste forhåndsdefinerte meldingen som det sjekkes for om eksisterer er `--end`. Denne sendes fra server i det serveren avslutter applikasjonen, og eksisterer for å slippe at det kastes exceptions hos klienten og for å kunne avslutte ting korrekt hos klienten.

## Forutsetninger for å kjøre
Prosjektet mitt er basert på Gradle og under er kommandoene som trengs for å bygge prosjektet samt hente dependencies.

Da jeg har tatt i bruk et par nye ting fra Java 8 har jeg også ført opp dette i `build.gradle` som et krav. Sørg for at dette er installert for deg som skal teste.

```bash
# For å bygge prosjektet, samt hente ned dependencies
./gradlew build

# For å produsere prosjektfiler for IntelliJ, skulle det være aktuelt
./gradlew idea
```
