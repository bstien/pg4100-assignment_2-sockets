package stibas14.pg4100.assignment2.server;

import org.junit.Before;
import org.junit.Test;
import stibas14.pg4100.assignment2.server.questions.FirstPublishedQuestion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FirstPublishedQuestionTest {

	private Book genericBook;
	private FirstPublishedQuestion question;

	@Before
	public void setUp() throws Exception
	{
		genericBook = new Book(1, "NYGÅRDSHAUG, GERT", "MENGELE ZOO", "978-82-02-28849-5", 455, 2008);
		question = new FirstPublishedQuestion(genericBook);
	}

	@Test
	public void it_should_parse_a_users_answer_correctly() throws Exception
	{
		// Positive answers.
		assertTrue(question.isPositiveAnswer("j"));
		assertTrue(question.isPositiveAnswer("J"));
		assertTrue(question.isPositiveAnswer("ja"));
		assertTrue(question.isPositiveAnswer("JA"));
		assertTrue(question.isPositiveAnswer("jA"));
		assertTrue(question.isPositiveAnswer("Ja"));
		assertTrue(question.isPositiveAnswer("yes"));
		assertTrue(question.isPositiveAnswer("YES"));
		assertTrue(question.isPositiveAnswer(" Y "));

		// Negative answers.
		assertFalse(question.isPositiveAnswer("n"));
		assertFalse(question.isPositiveAnswer("N"));
		assertFalse(question.isPositiveAnswer("A"));
	}

	@Test
	public void it_should_recognize_a_correct_answer() throws Exception
	{
		// Correct year given in question.
		question = spy(new FirstPublishedQuestion(genericBook));
		doAnswer(invocation -> 2008).when(question).selectFauxYear();

		// User answers correctly.
		assertTrue(question.isCorrectAnswer("ja"));

		// Wrong year given in question.
		question = spy(new FirstPublishedQuestion(genericBook));
		doAnswer(invocation -> 1945).when(question).selectFauxYear();

		// User answers correctly.
		assertTrue(question.isCorrectAnswer("nei"));
	}

	@Test
	public void it_should_recognize_a_wrong_answer() throws Exception
	{
		// Correct year given in question.
		question = spy(new FirstPublishedQuestion(genericBook));
		doAnswer(invocation -> 2008).when(question).selectFauxYear();

		// User answers wrong.
		assertFalse(question.isCorrectAnswer("nei"));

		// Wrong year given in question.
		question = spy(new FirstPublishedQuestion(genericBook));
		doAnswer(invocation -> 1945).when(question).selectFauxYear();

		// User answers wrong.
		assertFalse(question.isCorrectAnswer("ja"));
	}
}