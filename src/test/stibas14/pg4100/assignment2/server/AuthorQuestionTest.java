package stibas14.pg4100.assignment2.server;

import org.junit.Before;
import org.junit.Test;
import stibas14.pg4100.assignment2.server.questions.AuthorQuestion;

import static org.junit.Assert.*;

public class AuthorQuestionTest {

	private Book genericBook;
	private AuthorQuestion question;

	@Before
	public void setUp() throws Exception
	{
		genericBook = new Book(1, "NYGÅRDSHAUG, GERT", "MENGELE ZOO", "978-82-02-28849-5", 455, 2008);
		question = new AuthorQuestion(genericBook);
	}

	@Test
	public void it_should_recognize_correct_answers_no_matter_what_case() throws Exception
	{
		String answer;

		answer = "NYGÅRDSHAUG, GERT";
		assertTrue(question.isCorrectAnswer(answer));

		answer = "nygårdshaug, gert";
		assertTrue(question.isCorrectAnswer(answer));
	}

	@Test
	public void it_should_recognize_correct_answers_even_if_first_names_appears_before_last_name_no_matter_what_case() throws Exception
	{
		String answer;

		answer = "GERT NYGÅRDSHAUG";
		assertTrue(question.isCorrectAnswer(answer));

		answer = "gert nygårdshaug";
		assertTrue(question.isCorrectAnswer(answer));
	}

	@Test
	public void it_should_recognize_correct_answers_even_if_comma_is_omitted_no_matter_what_case() throws Exception
	{
		String answer;

		answer = "NYGÅRDSHAUG GERT";
		assertTrue(question.isCorrectAnswer(answer));

		answer = "nygårdshaug gert";
		assertTrue(question.isCorrectAnswer(answer));
	}

	@Test
	public void it_should_return_correct_question_string_based_on_books_title() throws Exception
	{
		String expected = "Hvem har skrevet boken 'MENGELE ZOO'?";
		String actual = question.getQuestionString();

		assertEquals(expected, actual);
	}

	@Test
	public void it_should_return_correct_answer() throws Exception
	{
		String expected = "NYGÅRDSHAUG, GERT";
		String actual = question.getCorrectAnswer();

		assertEquals(expected, actual);
	}
}