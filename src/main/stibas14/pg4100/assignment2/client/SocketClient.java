package stibas14.pg4100.assignment2.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class SocketClient {

	public static void main(String[] args) throws IOException
	{
		try (Socket socket = new Socket("localhost", 7575);
			 DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
			 DataInputStream inputStream = new DataInputStream(socket.getInputStream());
			 Scanner keyboard = new Scanner(System.in);
		)
		{
			while (!socket.isClosed())
			{
				String message = inputStream.readUTF();

				// Check if server wants to end the communication.
				if ( message.equals("--end") )
				{
					System.out.print(inputStream.readUTF());
					socket.close();
					break;
				}

				System.out.print(message);
				String userIn = keyboard.nextLine();

				outputStream.writeUTF(userIn);
				outputStream.flush();
			}
		}
	}
}
