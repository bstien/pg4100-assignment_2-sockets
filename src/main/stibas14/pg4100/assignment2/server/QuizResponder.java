package stibas14.pg4100.assignment2.server;

import stibas14.pg4100.assignment2.server.questions.Question;
import stibas14.pg4100.assignment2.server.questions.QuestionFactory;

import java.net.Socket;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuizResponder implements Runnable {

	private final QuestionFactory questionFactory;

	private Socket socket;

	private boolean quizActive;

	public QuizResponder(Socket socket, List<Book> books)
	{
		this.socket = socket;
		this.quizActive = true;
		this.questionFactory = new QuestionFactory(books);
	}

	@Override
	public void run()
	{
		try (SocketIOHandler io = new SocketIOHandler(socket))
		{
			String answer = io
					.write(welcomeMessage())
					.write("Ønsker du å delta? [j/n] : ")
					.send()
					.read();
			shouldContinue(answer);

			while (quizActive)
			{
				// Get a random question.
				Question question = questionFactory.makeRandomQuestion();

				// Present user with question and retrieve answer.
				answer = io.write(question.getQuestionString() + " : ").send().read();

				// Check if answer is correct and print message to user.
				checkAnswer(question, answer, io);

				// Prompt user to continue.
				answer = io.write("\nØnsker du å fortsette? (j/n) : ").send().read();
				shouldContinue(answer);
			}

			// End connection with client. This is a known string which has
			// to be implemented and recognized by the client.
			io.write("--end").send();
			io.write("Takk for nå!").send();

			// Close the socket.
			socket.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void shouldContinue(String answer)
	{
		quizActive = isPositive(answer);
	}

	private void checkAnswer(Question question, String answer, SocketIOHandler io)
	{
		if ( question.isCorrectAnswer(answer) )
		{
			io.write("Riktig!");
		}
		else
		{
			io.write("Feil... Korrekt svar er ");
			io.write(question.getCorrectAnswer());
		}
	}

	private boolean isPositive(String input)
	{
		String regex =
				"^\\s*(ja?|y(es)?)\\s*";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(input);

		return matcher.matches();
	}

	private String welcomeMessage()
	{
		return "+-----------------------+\n|  Velkommen til quiz!  |\n+-----------------------+\n\n";
	}
}