package stibas14.pg4100.assignment2.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketIOHandler implements AutoCloseable {

	private DataInputStream inputStream;
	private DataOutputStream outputStream;
	private StringBuilder stringBuilder;

	public SocketIOHandler(Socket socket) throws IOException
	{
		inputStream = new DataInputStream(socket.getInputStream());
		outputStream = new DataOutputStream(socket.getOutputStream());
		stringBuilder = new StringBuilder();
	}

	public String read() throws IOException
	{
		return inputStream.readUTF();
	}

	public SocketIOHandler write(String append)
	{
		stringBuilder.append(append);

		// Return this so it's easy to chain commands.
		return this;
	}

	public SocketIOHandler send() throws IOException
	{
		outputStream.writeUTF(stringBuilder.toString());
		outputStream.flush();

		// Empty the StringBuilder and trim its underlying buffer.
		stringBuilder.setLength(0);
		stringBuilder.trimToSize();

		return this;
	}

	@Override
	public void close() throws Exception
	{
		inputStream.close();
		outputStream.close();
	}
}
