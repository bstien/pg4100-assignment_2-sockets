package stibas14.pg4100.assignment2.server;

import stibas14.pg4100.assignment2.server.db.BookHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AppServer {

	// Change these to reflect actual setup.
	private String databaseUser = "root";

	private String databasePassword = "";

	private List<Book> books;

	private ExecutorService executorService;


	public AppServer()
	{
		initBookHandlerAndFetchBooks();
		executorService = Executors.newCachedThreadPool();
	}

	public static void main(String[] args)
	{
		AppServer appServer = new AppServer();
		appServer.start();
	}

	/**
	 * Fetch all books in database before we start accepting connections from
	 * clients.
	 */
	private void initBookHandlerAndFetchBooks()
	{
		try (BookHandler bookHandler = new BookHandler(databaseUser, databasePassword))
		{
			books = bookHandler.fetchAllBooks();
		} catch (Exception e)
		{
			System.out.println("Error fetching books from database.");
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * An infinite loop that accept client connections and starts quizzes.
	 */
	private void start()
	{
		try (ServerSocket serverSocket = new ServerSocket(7575))
		{
			while (true)
			{
				Socket socket = serverSocket.accept();
				QuizResponder quizResponder = new QuizResponder(socket, books);

				executorService.execute(quizResponder);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			executorService.shutdown();
		}
	}
}
