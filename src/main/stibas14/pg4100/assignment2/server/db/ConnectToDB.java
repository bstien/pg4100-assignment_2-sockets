package stibas14.pg4100.assignment2.server.db;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectToDB implements AutoCloseable {

	private String host;

	private String database;

	private String user;

	private String password;

	private int port;

	private Connection connection;

	private MysqlDataSource dataSource;

	public ConnectToDB(String host, int port, String database, String user, String password) throws SQLException
	{
		this.host = host;
		this.port = port;
		this.database = database;
		this.user = user;
		this.password = password;

		initializeConnection();
	}

	public ConnectToDB(String host, String database, String user, String password) throws SQLException
	{
		this(host, 3306, database, user, password);
	}

	/**
	 * Initialize and open the connection.
	 *
	 * @throws SQLException
	 */
	private void initializeConnection() throws SQLException
	{
		dataSource = new MysqlDataSource();

		String jdbcUrl = "jdbc:mysql://" + host + ":" + port + "/" + database;

		dataSource.setUrl(jdbcUrl);
		dataSource.setUser(user);
		dataSource.setPassword(password);

		connection = dataSource.getConnection();
	}

	/**
	 * Returns the DB-connection.
	 *
	 * @return The connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException
	{
		if ( connection == null || connection.isClosed() )
		{
			connection = null;
			initializeConnection();
		}

		return connection;
	}

	/**
	 * Closes the connection.
	 *
	 * @throws Exception
	 */
	@Override
	public void close() throws Exception
	{
		connection.close();
	}
}
