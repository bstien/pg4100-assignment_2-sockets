package stibas14.pg4100.assignment2.server.db;

import stibas14.pg4100.assignment2.server.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Use this class to retrieve books from a given database.
 */
public class BookHandler implements AutoCloseable {

	private ConnectToDB connectToDB;

	private PreparedStatement allBooksStatement;

	private Connection connection;

	private String bookTable = "bokliste";

	public BookHandler(String user, String password) throws SQLException
	{
		initConnection(user, password);
		initStatements();
	}

	/**
	 * Initialize statements that are needed for this class to function.
	 *
	 * @throws SQLException
	 */
	private void initStatements() throws SQLException
	{
		String allBooks = "SELECT * FROM " + this.bookTable;
		allBooksStatement = connection.prepareStatement(allBooks);
	}

	/**
	 * Initialize the connection to the database given a username and a
	 * password.
	 *
	 * @param user
	 * @param password
	 * @throws SQLException
	 */
	private void initConnection(String user, String password) throws SQLException
	{
		// As defined in the assignment.
		String host = "localhost";
		String database = "pg4100innlevering2";

		this.connectToDB = new ConnectToDB(host, database, user, password);
		this.connection = connectToDB.getConnection();
	}

	/**
	 * Retrieve all books stored in the database.
	 *
	 * @return A list of books from the DB
	 * @throws SQLException
	 */
	public List<Book> fetchAllBooks() throws SQLException
	{
		ArrayList<Book> books = new ArrayList<>();

		ResultSet result = allBooksStatement.executeQuery();

		while (result.next())
		{
			int id = result.getInt("id");
			String author = result.getString("forfatter");
			String title = result.getString("tittel");
			String isbn = result.getString("ISBN");
			int pages = result.getInt("sider");
			int firstPublished = result.getInt("utgitt");

			Book book = new Book(id, author, title, isbn, pages, firstPublished);
			books.add(book);
		}

		return books;
	}

	/**
	 * Close the database connection and all open statements.
	 *
	 * @throws SQLException
	 */
	@Override
	public void close() throws Exception
	{
		allBooksStatement.close();

		connectToDB.close();
	}

}
