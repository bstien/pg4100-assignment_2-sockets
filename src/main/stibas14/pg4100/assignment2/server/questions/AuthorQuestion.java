package stibas14.pg4100.assignment2.server.questions;

import stibas14.pg4100.assignment2.server.Book;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorQuestion implements Question {

	private Book book;

	private String fullName;

	private String firstName;

	private String lastName;

	public AuthorQuestion(Book book)
	{
		this.book = book;
		fullName = book.getAuthor().replaceAll("\\.", "\\\\.");

		String[] nameArr = fullName.split(", ");
		firstName = nameArr[1].trim();
		lastName = nameArr[0].trim();
	}

	@Override
	public boolean isCorrectAnswer(String answer)
	{
		// Matches these combinations, including lowercase,
		// given string "DOE, JOHN":
		// "DOE, JOHN", "DOE JOHN", "JOHN DOE"
		String regex = "^((" + fullName + ")|" +
				"(" + firstName + " " + lastName + ")|" +
				"(" + lastName + " " + firstName + "))\\s*$";

		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
		Matcher matcher = pattern.matcher(answer);

		return matcher.matches();
	}

	@Override
	public String getCorrectAnswer()
	{
		return book.getAuthor();
	}

	@Override
	public String getQuestionString()
	{
		String prefix = "Hvem har skrevet boken '";
		String question = prefix + book.getTitle() + "'?";
		return question;
	}
}
