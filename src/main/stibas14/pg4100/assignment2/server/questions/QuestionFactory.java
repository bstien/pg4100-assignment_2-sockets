package stibas14.pg4100.assignment2.server.questions;

import stibas14.pg4100.assignment2.server.Book;

import java.util.List;
import java.util.Random;

public class QuestionFactory {

	private List<Book> books;

	private Random random;

	public QuestionFactory(List<Book> books)
	{
		if ( books.isEmpty() )
		{
			throw new IllegalArgumentException("List of books can't be empty.");
		}
		this.books = books;
		this.random = new Random();
	}

	/**
	 * Make an AuthorQuestion based on a random book.
	 *
	 * @return An AuthorQuestion
	 */
	public Question makeAuthorQuestion()
	{
		Book book = randomBook();

		return new AuthorQuestion(book);
	}

	/**
	 * Make a FirstPublishedQuestion based on a random book.
	 *
	 * @return A FirstPublishedQuestion
	 */
	public Question makeFirstPublishedQuestion()
	{
		Book book = randomBook();

		return new FirstPublishedQuestion(book);
	}

	/**
	 * Make a random question based on a random book.
	 *
	 * @return A question
	 */
	public Question makeRandomQuestion()
	{
		int whichQuestion = random.nextInt(2);
		Question question = null;

		switch (whichQuestion)
		{
			case 0:
				question = makeFirstPublishedQuestion();
				break;
			case 1:
				question = makeAuthorQuestion();
				break;
		}

		return question;
	}

	/**
	 * Choose a random book.
	 *
	 * @return A book
	 */
	private Book randomBook()
	{
		int listSize = books.size();
		int randomIndex = random.nextInt(listSize);

		return books.get(randomIndex);
	}
}
