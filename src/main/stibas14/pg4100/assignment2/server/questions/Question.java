package stibas14.pg4100.assignment2.server.questions;

/**
 * An interface to be used by questions for a quiz.
 */
public interface Question {
	boolean isCorrectAnswer(String answer);
	String getCorrectAnswer();
	String getQuestionString();
}
