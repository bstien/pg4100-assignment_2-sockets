package stibas14.pg4100.assignment2.server.questions;

import stibas14.pg4100.assignment2.server.Book;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstPublishedQuestion implements Question {

	private Book book;

	private int fauxYear = -1;

	public FirstPublishedQuestion(Book book)
	{
		this.book = book;
	}

	public int selectFauxYear()
	{
		int high = book.getFirstPublished() + 5;
		int low = book.getFirstPublished() - 5;

		int year = new Random().nextInt(high - low) + low;
		return year;
	}

	@Override
	public boolean isCorrectAnswer(String answer)
	{
		setFauxYear();

		boolean positiveAnswer = isPositiveAnswer(answer);
		return positiveAnswer == (fauxYear == book.getFirstPublished());
	}

	public boolean isPositiveAnswer(String answer)
	{
		String regex =
				"^\\s*(ja?|y(es)?)\\s*";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(answer);

		return matcher.matches();
	}

	private void setFauxYear()
	{
		if ( this.fauxYear == -1 )
		{
			this.fauxYear = selectFauxYear();
		}
	}

	@Override
	public String getCorrectAnswer()
	{
		return "" + book.getFirstPublished();
	}

	@Override
	public String getQuestionString()
	{
		setFauxYear();
		return "Ble boken '" + book.getTitle() + "' utgitt i " + fauxYear + "? [j/n]";
	}
}
