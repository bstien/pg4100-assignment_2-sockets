package stibas14.pg4100.assignment2.server;

public class Book {

	private int id;

	private String author;

	private String title;

	private String isbn;

	private int pages;

	private int firstPublished;

	public Book(int id, String author, String title, String isbn, int pages, int firstPublished)
	{
		this.id = id;
		this.author = author;
		this.title = title;
		this.isbn = isbn;
		this.pages = pages;
		this.firstPublished = firstPublished;
	}

	public int getId()
	{
		return id;
	}

	public String getAuthor()
	{
		return author;
	}

	public String getTitle()
	{
		return title;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public int getPages()
	{
		return pages;
	}

	public int getFirstPublished()
	{
		return firstPublished;
	}
}
